
## Ce projet contient les anciens packages du Nexus Repository Public (Forge MTE).

### Récupération manuelle

Les packages Descartes peuvent être récupérés manuellement directement à partir du menu de gauche **"Packages & Registries"**:
https://gitlab-forge.din.developpement-durable.gouv.fr/pub/geomatique/repository-public/-/packages.

### Récupération depuis un projet Maven

Exemple de configuration à mettre dans le fichier "pom.xml":

    <dependencies>
        ....
        <dependency>
            <groupId>fr.gouv.e2.descartes</groupId>
            <artifactId>descartes-library-java</artifactId>
            <version>5.1.3-1</version>
            <exclusions>
                <exclusion>
                    <artifactId>avalon-framework</artifactId>
                    <groupId>avalon-framework</groupId>
                </exclusion>
                <exclusion>
                    <artifactId>logkit</artifactId>
                    <groupId>logkit</groupId>
                </exclusion>
                <exclusion>
                    <artifactId>servlet-api</artifactId>
                    <groupId>javax.servlet</groupId>
                </exclusion>
                <exclusion>
                    <artifactId>log4j</artifactId>
                    <groupId>log4j</groupId>
                </exclusion>
            </exclusions>
        </dependency>

        <dependency>
            <groupId>fr.gouv.e2.descartes</groupId>
            <artifactId>descartes-library-javascript</artifactId>
            <version>5.1.3-1</version>
            <type>pom</type>
        </dependency>

        <dependency>
            <groupId>fr.gouv.e2.descartes</groupId>
            <artifactId>descartes-samples-commun</artifactId>
            <version>5.1.3-1</version>
            <type>tar.gz</type>
            <classifier>gazetteer</classifier>
        </dependency>
    <dependencies>

    <repositories>
      <repository>
        <id>gitlab-maven-nexus-repo-public</id>
        <url>https://gitlab-forge.din.developpement-durable.gouv.fr/api/v4/projects/17010/packages/maven</url>
      </repository>
    </repositories>

    <plugins>
        ...
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-dependency-plugin</artifactId>
            <version>3.0.1</version>
            <executions>
                <execution>
                    <id>unpack-descartes-javascript-sources</id>
                    <phase>prepare-package</phase>
                    <goals>
                        <goal>unpack</goal>
                    </goals>
                    <configuration>
                        <artifact>fr.gouv.e2.descartes:descartes-library-javascript:5.1.3-1:tar.gz</artifact>
                        <outputDirectory>${project.build.directory}/${project.artifactId}</outputDirectory>
                    </configuration>
                </execution>
                <execution>
                    <id>unpack-descartes-commun-gazetteer</id>
                    <phase>prepare-package</phase>
                    <goals>
                        <goal>unpack</goal>
                    </goals>
                    <configuration>
                        <artifact>fr.gouv.e2.descartes:descartes-samples-commun:5.1.3-1:tar.gz:gazetteer</artifact>
                        <outputDirectory>${project.build.directory}/${project.artifactId}/descartes</outputDirectory>
                    </configuration>
                </execution>
            </executions>
        </plugin>
        ...
    </plugins>
    

